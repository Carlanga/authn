from django.urls import path
from django.views.generic import TemplateView

app_name = 'manifest'
urlpatterns = [
    path('',TemplateView.as_view(template_name='root.html')),
    path('grid/', TemplateView.as_view(template_name='cssgrid.html')),
    path('fb/', TemplateView.as_view(template_name='facebook.html')),
    path('author/',TemplateView.as_view(template_name='author/carlosgortaris.html')),
    path('meetings/',TemplateView.as_view(template_name='meetings.html')),
]

