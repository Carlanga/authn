from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic
from rest_framework.views import APIView
from rest_framework.response import Response
from manifest.models import Quote, Author

class IndexView(generic.ListView):
    template_name = 'manifest/root.html'

class QuoteView(generic.ListView):
    model = Quote

class AuthorView(generic.ListView):
    model = Author

class LangView(APIView):
    def get(self, request):
        content = { 'message': 'hallo' }
        return Response(content)
