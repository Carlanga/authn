from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token
from manifest import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('polls/', include('polls.urls')),
    path('manifest/', include('manifest.urls')),
    url(r'^auth-jwt/', obtain_jwt_token),
    url(r'^auth-jwt-refresh/', refresh_jwt_token),
    url(r'^auth-jwt-verify/', verify_jwt_token),
    path('lang/', views.LangView.as_view(), name='lang'),
    path('quote/', views.QuoteView.as_view(), name='quote'),
    path('blog/', include('blog.urls', namespace='blog')),
    url(r'^tinymce/', include('tinymce.urls')),
]
