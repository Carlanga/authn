from django import template
from ..models import Post

register = template.Library()

@register.simple_tag
def published_posts():
    return Post.published.count()

@register.simple_tag
def success_rate():
    return round ( Post.published.count() * 100 / Post.objects.all().count(), 1)
