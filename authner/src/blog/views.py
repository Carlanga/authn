from django.shortcuts import render, get_object_or_404
from .models import Post, Comment
from .forms import CommentForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from taggit.models import Tag
from django.db.models import Count

def post_list(request, tag_slug=None):
    object_list = Post.published.all()
    tag = None
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = object_list.filter(tags__in=[tag])
    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return render(request,
        'blog/post/list.html',
        {'page': page, 
         'posts': posts,
         'tag': tag})

def post_detail(request, year, month, day, post):
    p = get_object_or_404(Post,
        slug=post,
        status='published',
        publish__year=year,
        publish__month=month,
        publish__day=day)
    
    # List of active comments
    comments = p.comments.filter(active=True)
    new_comment = None
    if request.method == 'POST':
        # A comment was POSTed
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            # Create comment object but don't save to db yet
            new_comment = comment_form.save(commit=False)
            new_comment.post = p
            # Save comment to db
            new_comment.save()
    else:
        comment_form = CommentForm()
    
    # similar post by tag sharing
    post_tag_ids    = p.tags.values_list('id', flat=True)
    similar_posts   = Post.published.filter(tags__in=post_tag_ids).exclude(id=p.id)
    similar_posts   = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags','-publish')[:4] 
    return render(request,
        'blog/post/detail.html',
        {'post': p,
         'comments': comments,
         'new_comment': new_comment,
         'comment_form': comment_form,
         'similar_posts': similar_posts})

