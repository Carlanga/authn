#!/bin/sh

echo -n "Running Django migration... "
docker exec -it authner-web python manage.py migrate
docker exec -it authner-web python manage.py createsuperuser
echo "OK"
