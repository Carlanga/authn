# Authner

Set of services on docker containers - web platform:

* Website.
* Database.
* API.
* Cache.
* Tests.

## TL; DR

Run `make` then visit `authner-web:8000/blog`.

## Did it worked? If not maybe check these requirements

### Networking

#### Ports

Host machine should have available the following TCP ports:

* 8000: Django website.
* 5432: PostgreSQL database.
* 4000: Flask API.
* 6379: Redis cache.

#### Hosts

Host machine should have these hostnames mapped to 127.0.0.1 (e.g. at `/etc/hosts`):

```bash
127.0.0.1 authner-web
127.0.0.1 authner-db
127.0.0.1 authner-api
127.0.0.1 authner-cache
```

## Usage

* Run `make`.

### Dutch Dictionary

* Get meaning of dutch word: `curl http://authner-api:4000/woordt?w={a dutch word}`. If no meaning has been set yet for the word, then the default phrase `no meaning set yet` will be shown (until there's a definition for such a state).
* Set meaning for a dutch word: `curl "http://authner-api:4000/woordt?w={a dutch word}&m={the meaning}"`

### Websites

#### Django

* Django, [`authner-web`](http://authner-web:8000).
* Image: Django (latest).
* Container name: `authner-web`.

### Database

* PostgreSQL database: `http://authner-db:5432`.
* Image: Postgres (latest).
* Container name: `authner-db`.

### Services

#### API

* Flask api, [`authner-api`](http://authner-api:4000).
* Image: Flask (latest).
* Container name:`authner-api`.

#### Cache

* Redis cache: `http://authner-cache:6379`.
* Image: Redis (latest).
* Container Name: `authner-cache`.

### Tests

Run `make tests`.
