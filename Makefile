.PHONY: all up backend frontend web init-web db api authenticate cache down clean build ssh-web ssh-db ssh-api ssh-cache test test-jwt logs log-web log-db log-api log-cache log-authenticate chromedriver chrome-firefox psql redis-cli getkey

all: up logs test

up: backend frontend

backend: db cache

frontend: web api authenticate ux

web:
	docker-compose up --detach web

init-web:
	./authner/bin/init.sh

db:
	docker-compose up --detach db && sleep 5

api:
	docker-compose up --detach api

authenticate:
	docker-compose up --detach authenticate

ux:
	docker-compose up --detach ux

cache:
	docker-compose up --detach cache

test: up chrome-firefox chromedriver test-jwt
	docker-compose up test && \
	./test/nw/run.sh authner-web-integration root.js && \
	./test/nw/run.sh authner-web-integration author.js && \
	./test/nw/run.sh authner-web-integration quote.js && \
	./test/bin/curls.sh ./test/resources/uris.local && \
	make chromedriver-down

test-jwt: web
	curl --request POST --url http://authner-web:8000/auth-jwt/ --header 'Content-Type: application/json' --data '{"username": "jwtuser", "password": "jwtpass1423"}'

chromedriver:
	{ ./node_modules/chromedriver/lib/chromedriver/chromedriver --port=4445 --url-base=/wd/hub & echo $$! > chromedriver.PID; }

chromedriver-down:
	if [ -f ./chromedriver.PID ] ; \
	then \
		kill -9 `cat chromedriver.PID` && rm chromedriver.PID ; \
	fi ;

chrome-firefox:
	cd test/nw && npm run nightwatch

down: chromedriver-down
	docker-compose down

clean: down
	docker rmi authn_web; docker rmi authn_api; docker rmi authn_authenticate; docker rmi authn_ux

build:
	docker-compose build

ssh-web: web
	docker exec -it authner-web /bin/bash

ssh-db: db
	docker exec -it authner-db /bin/bash

ssh-api: api
	docker exec -it authner-api /bin/bash

ssh-authenticate: authenticate
	docker exec -it authner-authenticate /bin/bash

ssh-ux:
	docker exec -it authner-ux /bin/bash

ssh-cache: cache
	docker exec -it authner-cache /bin/bash

log-web: web
	docker logs authner-web

log-db: db
	docker logs authner-db

log-api: api
	docker logs authner-api

log-authenticate: authenticate
	docker logs authner-authenticate

log-ux: ux
	docker logs authner-ux

log-cache: cache
	docker logs authner-cache

logs: log-web log-db log-api log-cache log-authenticate
	@echo "LOG: `date`"

psql: db
	docker exec -it authner-db psql -U authner -d authnerdb

redis-cli: cache
	docker exec -it authner-cache redis-cli

getkey: cache
	docker exec -it authner-cache redis-cli get ${KEY}
