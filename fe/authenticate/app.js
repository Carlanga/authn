/**
 * api/app.js
 * exports an express app.
 */
'use strict';

const express   = require('express');
const PORT      = 3000;
const HOST      = '0.0.0.0';

const app = express();

const bodyParser = require('body-parser');

//middleware to parse requests of extended urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

//middleware to parse requests of content-type - application/json
app.use(bodyParser.json());

//import router with endpoints definitions
const routes = require('./api/routes');
//attach router as a middleware
app.use(routes);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

module.exports = app;
