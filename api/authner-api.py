import time
import redis
from flask import Flask
from flask import request
from flask import jsonify

app = Flask(__name__)
cache = redis.Redis(host='authner-cache', port=6379)

@app.route('/woordt')
def woordt():
    woordt  = request.args.get('w')
    meaning = request.args.get('m')
    lookup  = get_woordt(woordt, meaning)
    return definition(woordt, lookup)

@app.route('/')
def list():
    words = {}
    for el in cache.keys(pattern='*'):
        keystr = el.decode('utf-8')
        if keystr != 'hits':
            words[keystr] = cache.get(el).decode('utf-8')
    return jsonify(words)

@app.route('/debug')
def count():
    count = get_hit_count()
    return 'Helld! I have been seen {} times.\n'.format(count)

def get_woordt(woordt, meaning):
    lookup = cache.get(woordt)
    if lookup == None:
        if meaning == None:
            return b'no meaning set yet'
        else:
            cache.set(woordt, meaning)
            return meaning
    else:
        if meaning != None:
            cache.set(woordt, meaning)
            return meaning
    return lookup.decode('utf-8')

def definition(woordt, meaning):
    return '{}: {}'.format(woordt, meaning)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)
