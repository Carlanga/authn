#!/usr/bin/env sh

set -eu

# Add python pip and bash
apk add --no-cache py-pip bash build-base python2-dev openssl-dev libffi-dev

# Install docker-compose via pip
pip install --no-cache-dir cffi docker-compose
docker-compose -v
