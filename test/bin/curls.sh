#!/bin/bash

FILE=$1

for uri in `cat $FILE`; do
    curl ${uri} 1>/dev/null 2>/dev/null;
    ECODE=$?
    if [ $ECODE -eq 0 ]; then 
        echo "OK ${uri}"
    else
        echo "-- ${uri}"
    fi 
done
