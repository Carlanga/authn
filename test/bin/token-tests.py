import unittest
import subprocess
import re
import os
import sys
import requests
import json
from pprint import pprint

class TestRunningMethods(unittest.TestCase):

    def setUp(self):
        self.uri        = 'http://authner-web:8000/auth-jwt/'
        self.username   = 'jwtuser'
        self.password   = 'jwtpass1423'
        self.token      = self.getToken()

    def getToken(self):
        hdr     = { 'Content-Type': 'application/json' }
        payload = { 'username': self.username, 'password': self.password }
        res     = requests.post(self.uri, headers=hdr, params=None, data=json.dumps(payload))
        content = res.json()
        return content['token']

    def test_token(self):
        print("TEST: get JWT token\n")
        hdr     = { 'Content-Type': 'application/json' }
        payload = { 'username': 'jwtuser', 'password': 'jwtpass1423' }
        res     = requests.post(self.uri, headers=hdr, params=None, data=json.dumps(payload))
        self.assertEqual( res.status_code, requests.codes.ok )
        self.assertEqual( res.headers['Content-Type'], 'application/json' )
        self.assertEqual( res.reason, 'OK')
        content = res.json()
        token = content['token']
        self.token = token
        self.assertEqual( len(token), 164 )

    def test_request(self):
        print("TEST: perform request using authN\n")
        uri = 'http://authner-web:8000/lang/'
        hdr = {
            'Content-Type':     'application/json',
            'Authorization':    'JWT {}'.format(self.token)
        }
        res = requests.get(uri, headers=hdr)
        content = res.json()
        self.assertEqual( 'hallo', content['message'] )

if __name__ == '__main__':
    unittest.main()
