import unittest
import subprocess
import re
import os
import sys
import pprint

class TestRunningMethods(unittest.TestCase):

    def test_header(self):
        path = os.path.dirname(os.path.realpath(__file__))
        urisfilename = os.path.join(path, r'endpoints.url')
        print("TEST: URIs response headers:")
        with open(urisfilename, 'rb') as f:
            lines = [x.decode('utf8').strip() for x in f.readlines()]
            for line in lines:
                try:
                    ok = self.uri_alive(line)
                except Exception as e:
                    print(e)
                    ok = False
                if ok:
                    print("\t{}: OK".format(line))
                else:
                    print("\t{}: FAIL".format(line))
            f.close()

    def uri_alive(self, uri):
        OKpattern   = '< HTTP/1.[01] (200 OK|302 Found)'
        found       = False
        try:
            cmnd = ['curl', '-v', uri]            
            output = subprocess.check_output(cmnd, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as exc:
            print("Status : FAIL", exc.returncode, exc.output)            
        else:
            if re.search(OKpattern, output.decode()):
                found = True
        self.assertTrue(found)
        return found

if __name__ == '__main__':
    unittest.main()
