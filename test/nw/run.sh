#!/usr/bin/env bash

ENVIR=$1
BASEDIR=$(dirname "$0")
BINARY=${BASEDIR}/../../node_modules/nightwatch/bin/nightwatch
CONFIGFILE=${BASEDIR}/conf.json
TESTFILE=${BASEDIR}/tests/$2

${BINARY} --env ${ENVIR} --config ${CONFIGFILE} --test ${TESTFILE}
