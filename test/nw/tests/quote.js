module.exports = {
    'Quote Title': function(client) {
        const title = 'h1';
        client
            .url('http://authner-web:8000/quote')
            .assert.elementPresent(title)
            .end();
    },
    'Quote Title Text': (ua) => {
        const title = 'h1';
        ua.url('http://authner-web:8000/quote')
        .assert.containsText(title, 'Quotes')
        .end();
    }
};
