const rootURL = 'http://authner-web:8000/manifest';
module.exports = {
    'Root' : function(client) {
        const title = 'p';
        client
            .url(rootURL)
            .assert.elementPresent(title)
            .end();
    },
    'Manifest text' : function(client) {
        client
            .url(rootURL)
            .waitForElementVisible('body', 1000)
            .waitForElementVisible('p', 1000)
            .assert.containsText('p', 'root of all')
            .end();
    }
};
